package clases

import kotlinx.serialization.SerialName

@kotlinx.serialization.Serializable
@SerialName("esport")
data class Esport(
    val esport: List<Usuari>
)

@kotlinx.serialization.Serializable
@SerialName("usuari")
data class Usuari(
    val esport: String,
    val temps: Int
)
