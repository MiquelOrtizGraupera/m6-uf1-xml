package clases

import kotlinx.serialization.SerialName
import nl.adaptivity.xmlutil.serialization.XmlElement

@kotlinx.serialization.Serializable
@SerialName("restaurant")
data class Restaurant(
    val type: String,
    @XmlElement(true) val name: String,
    @XmlElement(true) val address: String,
    @XmlElement(true) val owner: String)
