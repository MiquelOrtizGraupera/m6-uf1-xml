package clases

import kotlinx.serialization.SerialName
import nl.adaptivity.xmlutil.serialization.XmlChildrenName
import nl.adaptivity.xmlutil.serialization.XmlElement

@kotlinx.serialization.Serializable
@SerialName("recipes")
data class Receptes(
    val recipies:List<Recepte>
)

@kotlinx.serialization.Serializable
@SerialName("recipe")
data class Recepte(
    val dificulty:String,
    @XmlElement(true) val name: String,
    @XmlChildrenName("ingredient", "","")
    @SerialName("ingredients") val ingredients: List<Ingredient>
)
@kotlinx.serialization.Serializable
@SerialName("ingredient")
data class Ingredient(
    val ammount: String,
    val unit: String,
    val name: String
)
