import clases.Restaurants
import nl.adaptivity.xmlutil.serialization.XML
import java.util.Scanner
import kotlin.io.path.Path
import kotlin.io.path.readText

fun main(){
    val xml = Path("src/main/resources/restaurants.xml").readText()
    val scan = Scanner(System.`in`)
    val restaurants: Restaurants = XML.decodeFromString(xml)

    println("Quin tipus de restaurant vols?")
    val tipus = scan.next()

    println(restaurants.restaurants.filter { it.type == tipus })
}