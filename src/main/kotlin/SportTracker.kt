import clases.Usuari
import nl.adaptivity.xmlutil.serialization.XML
import java.util.Scanner
import kotlin.io.path.Path
import kotlin.io.path.writeText

fun main() {
    val userList: MutableList<Usuari> = mutableListOf()
    val scan = Scanner(System.`in`)
    var boolean = true
    var total:Int = 0
    while (boolean){
        if(userList.isNotEmpty()){
            userList.iterator().forEach {
                u ->  total = u.temps.plus(total)
                println(total)
            }
        }
        println("Quin esport has fet? ")
        val esport = scan.next()

        println("Quant de temps l'has practicat?")
        val temps = scan.nextInt()

        val user = Usuari(esport, temps)
        userList.add(user)
        println("Informació enregistrada")


        println("vols sortir? si/no")
        val sortir = scan.next()
        if(sortir.contains("si")){
            val xmlUser = XML.Companion.encodeToString(userList)
            Path("src/main/resources/esport.xml").writeText(xmlUser)
            boolean = false
            println(userList)
        }
    }
}