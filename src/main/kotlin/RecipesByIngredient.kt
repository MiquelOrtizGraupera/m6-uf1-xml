import clases.Receptes
import nl.adaptivity.xmlutil.serialization.XML
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.readText

fun main(){
    val scan = Scanner(System.`in`)
    val xml = Path("src/main/resources/receptes.xml").readText()

   val recepte: Receptes = XML.decodeFromString(xml)

    println(recepte)

    println("Escriu el nom de la recepta:")
    val recep = scan.next()
    //Filtre per nom de recepte
    val filtre = recepte.recipies.filter { it.name == recep }
    println(filtre)

    //Filtre per ingredient
    println("Escriu el nom de l'ingredient:")
    val ingred = scan.next()
    val filterIngredient = recepte.recipies.filter {
        it.ingredients.any { it.name == ingred }  }.sortedBy { it.dificulty }
    println(filterIngredient)
}