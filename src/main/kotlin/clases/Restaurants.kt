package clases

import kotlinx.serialization.SerialName

@kotlinx.serialization.Serializable
@SerialName("restaurants")
data class Restaurants(
    val restaurants : List<Restaurant>
)
