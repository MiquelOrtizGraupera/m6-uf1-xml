import clases.Restaurant
import nl.adaptivity.xmlutil.serialization.XML
import kotlin.io.path.Path
import kotlin.io.path.readText

fun main() {
    val xml = Path("src/main/resources/restaurant.xml").readText()
    val restaurant: Restaurant = XML.decodeFromString(xml)
    println(restaurant)
}